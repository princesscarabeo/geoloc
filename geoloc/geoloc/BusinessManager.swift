//
//  BusinessManager.swift
//  geoloc
//
//  Created by Princess Carabeo on 18/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit
import CoreLocation

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}

class Business: NSObject, MKAnnotation {
    var id: String!
    var name: String!
    var radius: CLLocationDistance!
    var loc:CLLocationCoordinate2D!
    var notifyOnEntry:Bool!
    var notifyOnExit:Bool!
    
    required init(data: JSON) {
        id = data["_id"].stringValue
        name = data["name"].stringValue
        radius = data["radius"].doubleValue
        loc = CLLocationCoordinate2D(latitude: data["loc"][1].doubleValue,longitude: data["loc"][0].doubleValue);
        notifyOnExit = true
        notifyOnEntry = true;
    }
    
    var title: String? {
        if name.isEmpty {
            return "No Note"
        }
        return name
    }
    
    var subtitle: String? {
        return "(\(coordinate.latitude), \(coordinate.longitude))"
    }
    
    var coordinate: CLLocationCoordinate2D {
        return loc
    }
}

class BusinessManager:NSObject {
    static let sharedInstance = BusinessManager()
    let baseURL = "https://freebirdapp.com/api/getbusiness?id=&lat=34.019454034&lng=-118.491191&radius=50"
    
    func getBusinessLocations(onCompletion: (Array<Business>) -> Void) {
        let route = baseURL
        RestApiManager.sharedInstance.makeHTTPGetRequest(route, onCompletion: { json, err in
            var businesses = [Business]()
            if let results = json["businesses"].array {
                for _result in results {
                    print ("test \(_result["loc"])")
                    if (_result["loc"].count>=2 && _result["radius"] != nil){
                        businesses.append(Business(data: _result));
                    }
                }
                dispatch_async(dispatch_get_main_queue(),{
                    onCompletion(businesses)
                })
            }
            
            
        })
    }
}