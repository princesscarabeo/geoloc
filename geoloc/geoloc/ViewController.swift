//
//  ViewController.swift
//  geoloc
//
//  Created by Princess Carabeo on 17/04/2016.
//  Copyright © 2016 Princess Carabeo. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import MapKit

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var indicator: UIImageView!
    
    var geotifications = [Business]()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.handleEnterRegionEvent(_:)), name: "Event_DidEnterRegion", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.handleExitRegionEvent(_:)), name: "Event_DidExitRegion", object: nil)
        
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        loadAllGeotifications()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleEnterRegionEvent(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let region = userInfo["data"] {
                for business in geotifications {
                    if business.id == region.identifier {
                        if UIApplication.sharedApplication().applicationState == .Active {
//                            showSimpleAlertWithTitle(nil, message: "Welcome to \(business.name)!", viewController: self)
                            indicator.image = UIImage(named: "arrive")
                        } else {
                            let notification = UILocalNotification()
                            notification.alertBody = "Welcome to \(business.name)!"
                            notification.soundName = "Default";
                            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
                        }
                        break;
                    }
                }
            }
        }

    }
    
    func handleExitRegionEvent(notification: NSNotification) {
        if UIApplication.sharedApplication().applicationState == .Active {
//            let message = "Bye!"
//            showSimpleAlertWithTitle(nil, message: message, viewController: self)
            indicator.image = UIImage(named: "left")
        } else {
            let notification = UILocalNotification()
            notification.alertBody = "Bye!"
            notification.soundName = "Default";
            UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        }
    }

    func loadAllGeotifications() {
        for business in self.geotifications {
            stopMonitoringGeotification(business)
            mapView.removeAnnotation(business)
            removeRadiusOverlayForGeotification(business)
        }
        
        BusinessManager.sharedInstance.getBusinessLocations { (businesses: Array<Business>) in
            self.geotifications = [];
            self.geotifications.appendContentsOf(businesses);
            var count = 0;
            
            if (self.geotifications.count>20){
                self.showSimpleAlertWithTitle("Warning", message: "Apple only allows 20 GeoFences to be monitored at a time! App detected \(self.geotifications.count). Only 20 locations will be displayed.", viewController: self)
            }
            
            for business in self.geotifications {
//                print (business.loc);
                if (count == min(self.geotifications.count,20)) {break}
                self.mapView.addAnnotation(business);
                self.addRadiusOverlayForGeotification(business)
                self.startMonitoringGeotification(business)
                count++
            }
            self.mapView.showAnnotations(self.mapView.annotations, animated: true);
        }
    }
    
    func removeGeotification(geotification: Business) {
        if let indexInArray = geotifications.indexOf(geotification) {
            geotifications.removeAtIndex(indexInArray)
        }
        mapView.removeAnnotation(geotification)
        removeRadiusOverlayForGeotification(geotification)
    }
    
    
    // MARK: MKMapViewDelegate
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        print ("Region changed! \(self.mapView.centerCoordinate)")
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "aBusinessNotification"
        if annotation is Business {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .Custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "close")!, forState: .Normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.lineWidth = 1.0
        circleRenderer.strokeColor = UIColor.redColor()
        circleRenderer.fillColor = UIColor.redColor().colorWithAlphaComponent(0.3)
        return circleRenderer
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geotification = view.annotation as! Business
        stopMonitoringGeotification(geotification)
        removeGeotification(geotification)
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
    }
    
    // MARK: Map overlay functions
    func addRadiusOverlayForGeotification(geotification: Business) {
        mapView?.addOverlay(MKCircle(centerCoordinate: geotification.coordinate, radius: geotification.radius))
    }
    
    func removeRadiusOverlayForGeotification(geotification: Business) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        if let overlays = mapView?.overlays {
            for overlay in overlays {
                if let circleOverlay = overlay as? MKCircle {
                    let coord = circleOverlay.coordinate
                    if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                        mapView?.removeOverlay(circleOverlay)
                        break
                    }
                }
            }
        }
    }

    
    // MARK: Others
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .AuthorizedAlways)
    }
    
    func regionWithGeotification(geotification: Business) -> CLCircularRegion {
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.id)
        region.notifyOnEntry = geotification.notifyOnEntry
        region.notifyOnExit = geotification.notifyOnExit
        return region
    }
    
    func startMonitoringGeotification(geotification: Business) {
        if !CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion) {
            showSimpleAlertWithTitle("Error", message: "Geofencing is not supported on this device!", viewController: self)
            return
        }
        if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
            showSimpleAlertWithTitle("Warning", message: "Please grant Geotify permission to access the device location.", viewController: self)
        }
        let region = regionWithGeotification(geotification)

        print("Monitoring start for region with ID: \(region.identifier)")
        locationManager.startMonitoringForRegion(region)
    }
    
    func stopMonitoringGeotification(geotification: Business) {
        for region in locationManager.monitoredRegions {
            if let circularRegion = region as? CLCircularRegion {
                if circularRegion.identifier == geotification.id {
                    locationManager.stopMonitoringForRegion(circularRegion)
                }
            }
        }
    }
    func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location Manager failed with the following error: \(error)")
    }
    
    //Additional
    func showSimpleAlertWithTitle(title: String!, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alert.addAction(action)
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
    
    func zoomToUserLocationInMapView(mapView: MKMapView) {
        if let coordinate = mapView.userLocation.location?.coordinate {
            let region = MKCoordinateRegionMakeWithDistance(coordinate, 10000, 10000)
            mapView.setRegion(region, animated: true)
        }
    }

}

